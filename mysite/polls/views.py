from django.http import HttpResponseRedirect, HttpResponse
from django.http import Http404
from django.shortcuts import render, get_object_or_404, redirect
from django.core.urlresolvers import reverse
from django.views import generic
from django.utils import timezone
from django.contrib.auth import logout
from polls.models import Choice, Question, Track
from django.contrib.auth import authenticate
from django.contrib.auth.models import User

def index(request):
    username = ""
    isLoggedIn = request.user.is_authenticated()
    if(isLoggedIn):
        username = request.user.get_username()
    latest_question_list = Question.objects.all().order_by('-pub_date')[:5]
    context = {'latest_question_list': latest_question_list, 'isLoggedIn': isLoggedIn, 'username': username}
    return render(request, 'polls/index.html', context)

class DetailView(generic.DetailView):
    model = Question
    template_name = 'polls/detail.html'
    def get_queryset(self):
        """
        Excludes any questions that aren't published yet.
        """
        return Question.objects.filter(pub_date__lte=timezone.now())

class ResultsView(generic.DetailView):
    model = Question
    template_name = 'polls/results.html'
    def get_queryset(self):
        """
        Excludes any questions that aren't published yet.
        """
        return Question.objects.filter(pub_date__lte=timezone.now())	

def logout_view(request):
    logout(request)
    return redirect('/polls/')

def vote(request, question_id):
    p = get_object_or_404(Question, pk=question_id)
    try:
        selected_choice = p.choice_set.get(pk=request.POST['choice'])
    except (KeyError, Choice.DoesNotExist):
        # Redisplay the question voting form.
        return render(request, 'polls/detail.html', {
            'question': p,
            'error_message': "You didn't select a choice.",
        })
    else:
        if(request.user.is_authenticated()):
            username = request.user.get_username()
            try:
                hasVoted = Track.objects.get(question = p, user=username)
            except Track.DoesNotExist:
                voteUser= Track(question = p, choice_id = selected_choice, user=username )
                voteUser.save()
                selected_choice.votes += 1
                selected_choice.save()
        # Always return an HttpResponseRedirect after successfully dealing
        # with POST data. This prevents data from being posted twice if a
        # user hits the Back button.
        return HttpResponseRedirect(reverse('polls:results', args=(p.id,)))
