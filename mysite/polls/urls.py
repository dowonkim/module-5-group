from django.conf.urls import patterns, url

from polls import views

urlpatterns = patterns('',
    url(r'^$', views.index, name='index'),
    url(r'^(?P<pk>\d+)/$', views.DetailView.as_view(), name='detail'),
    url(r'^(?P<pk>\d+)/results/$', views.ResultsView.as_view(), name='results'),
    url(r'^logout/$', views.logout_view, name='logout_view'),
    url(r'^(?P<question_id>\d+)/vote/$', views.vote, name='vote'),
)
TEMPLATE_CONTEXT_PROCESSORS = (
    'django.core.context_processors.request',
)