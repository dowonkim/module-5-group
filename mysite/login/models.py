from django.db import models

class Users(models.Model):
    username = models.CharField(max_length=20)
    password = models.CharField(max_length=250)

class Votes(models.Model):
    username_id = models.IntegerField()
    question_id = models.IntegerField()